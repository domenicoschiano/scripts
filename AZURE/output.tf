output "public_ip" {
  value = azurerm_public_ip.terraform-public.ip_address
}

output "Service_key" {
  value = azurerm_express_route_circuit.terraform-express-route.service_key
}