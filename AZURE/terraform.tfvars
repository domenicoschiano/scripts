## INSERT THE NAME of EXPRESS ROUTE RESOURCE GROUP
exr_rg = "xxxxx"

## INSERT THE NAME of MAIN VNET
main_vnet_name = "xxxxx"

## INSERT THE NAME of EXPRESS ROUTE VIRTUAL GATEWAY
ex_vgw_name = "xxxxx"

## INSERT THE LOCATION of EXPRESS ROUTE RESOURCE GROUP
exr_rg_location = "xxxxx"

## INSERT THE NETWORK AND MASK of MAIN VNET (example 192.168.0.0/16)
main_vnet_network = ["x.x.x.x/x"]

## INSERT THE DEDCIATED SUBNET of EXPRESS ROUTE VIRTUAL GATEWAY (>=/27) (example 192.168.1.0/27)
er_gw_subnet = ["x.x.x.x/x"]

## INSERT THE NETWORK of PRIMARY BGP PEER (AZ WILL ALWAYS HAVE THE HIGHEST) (example 10.17.17.0/30)
primary_peer_bgp_network = "x.x.x.x/x"

## INSERT THE NETWORK of SECONDARY BGP PEER (AZ WILL ALWAYS HAVE THE HIGHEST) (example 10.17.18.0/30)
secondary_peer_bgp_network = "x.x.x.x/x"

## INSERT REMOTE BGP ASN (example 65100)
remote_as = "xxxxx"

## INSERT VLAN ID (Dot1.q), IT MUST MATCH WITH THE ONE CONFIGURED INTO HUB (example 300)
vlan_id = "xxx"