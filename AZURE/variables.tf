variable "exr_rg" {
    type = string
    description = "EXPRESS ROUTE RESOURCE GROUP NAME"
}

variable "main_vnet_name" {
    type = string
    description = "MAIN VNET NAME"
}

variable "ex_vgw_name" {
    type = string
    description = "Express Route Virtual Gateway Name"
}

variable "exr_rg_location" {
    type = string
    description = "Location of Express Route Resource Group"
}

variable "main_vnet_network" {
    type = list
    description = "MAIN VNET Subnet"
}

variable "er_gw_subnet" {
    type = list
    description = "Express Route Virtual Gateway Subnet"
}

variable "primary_peer_bgp_network" {
    type = string
    description = "Primary BGP Peer Network"
}

variable "secondary_peer_bgp_network" {
    type = string 
    description = "Secondary BGP Peer Network"
}

variable "remote_as" {
    type = string
    description = "Remote BGP AS number"
}

variable "vlan_id" {
    type = string
    description = "Dot1.q to match remote site"
}