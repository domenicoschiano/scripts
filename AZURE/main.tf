## DEFINE PROVIDER
provider "azurerm" {
  features {}
}

## DEFINE RESOURCE GROUP
resource "azurerm_resource_group" "terraform-rg" {
  name     = var.exr_rg
  location = var.exr_rg_location
}

## DEFINE MAIN VNET
resource "azurerm_virtual_network" "terraform-vnet" {
  name                = var.main_vnet_name
  resource_group_name = azurerm_resource_group.terraform-rg.name
  location            = azurerm_resource_group.terraform-rg.location
  address_space       = var.main_vnet_network

}

## DEFINE SUBNET FOR EXPRESS ROUTE GATEWAY
resource "azurerm_subnet" "GatewaySubnet" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.terraform-rg.name
  virtual_network_name = azurerm_virtual_network.terraform-vnet.name
  address_prefixes     = var.er_gw_subnet
}

## DEFINE PUBLIC IP ADDRESS FOR ERGW
resource "azurerm_public_ip" "terraform-public" {
  name                = "ergw-public"
  location            = azurerm_resource_group.terraform-rg.location
  resource_group_name = azurerm_resource_group.terraform-rg.name

  allocation_method = "Dynamic"
}

## DEFINE VIRTUAL GATEWAY FOR ER
resource "azurerm_virtual_network_gateway" "terraform-exgw" {
  name                = var.ex_vgw_name
  location            = azurerm_resource_group.terraform-rg.location
  resource_group_name = azurerm_resource_group.terraform-rg.name

  type     = "ExpressRoute"
  sku      = "Standard"
  
  ip_configuration {
    name                          = "${var.ex_vgw_name}-net"
    public_ip_address_id          = azurerm_public_ip.terraform-public.id
    subnet_id                     = azurerm_subnet.GatewaySubnet.id
  }
}

## DEFINE Express Route Virtual Gateway Connection
resource "azurerm_virtual_network_gateway_connection" "terraform-exgw-conx" {
  location                   = azurerm_resource_group.terraform-rg.location
  name                       = "${var.ex_vgw_name}-conx"
  resource_group_name        = azurerm_resource_group.terraform-rg.name
  type                       = "ExpressRoute"
  virtual_network_gateway_id = azurerm_virtual_network_gateway.terraform-exgw.id
  express_route_circuit_id   = azurerm_express_route_circuit.terraform-express-route.id
  routing_weight             = 100

}

## DEFINE Express Route Circuit
resource "azurerm_express_route_circuit" "terraform-express-route" {
  name                  = "terraform-express-route"
  resource_group_name   = azurerm_resource_group.terraform-rg.name
  location              = azurerm_resource_group.terraform-rg.location
  service_provider_name = "Colt Ethernet"
  peering_location      = "Dublin"
  bandwidth_in_mbps     = 10000

  sku {
    tier   = "Premium"
    family = "MeteredData"
  }

  allow_classic_operations = false

  tags = {
    environment = "Production"
  }
}

## DEFINE Express Route Peering
resource "azurerm_express_route_circuit_peering" "terraform_exr_circuit" {
  peering_type                  = "AzurePrivatePeering"
  express_route_circuit_name    = azurerm_express_route_circuit.terraform-express-route.name
  resource_group_name           = azurerm_resource_group.terraform-rg.name
  peer_asn                      = var.remote_as 
  primary_peer_address_prefix   = var.primary_peer_bgp_network
  secondary_peer_address_prefix = var.secondary_peer_bgp_network
  vlan_id                       = var.vlan_id
  shared_key                    = "MuLt1Cl0Ud"

  microsoft_peering_config {
    advertised_public_prefixes = []
  }
}
