## INSERT PROVIDER AK;SK;TOKEN
access_key = "XXXXX"
secret_key = "XXXXX"
token      = "XXXXX"

## INSERT VPC SUBNET (example 192.168.1.0/24)
vpc_subnet_cidr = "x.x.x.x/x"

## INSERT PRIVATE SUBNET (example 192.168.1.0/25)
private_subnet_cidr = "x.x.x.x/x"

## INSERT AMAZON PEER IP ADDRESS VPC NEEDED TO ESTABILISH BGP WITH HUB (example 10.17.17.2/30)
primary_amazon_address = "x.x.x.x/x"

## INSERT HUB PEER IP ADDRESS AND MASK NEEDED TO ESTABILISH BGP (example 10.17.17.1/30)
primary_customer_address = "x.x.x.x/x"

## INSERT MTU for VLINK if >1500 must enable JUMBO frame (example 1500)
mtu = x

## INSERT AWS BGP ASN (example 65100)
bgp_asn = x

## INSERT VLAN ID (Dot1.q) must match with the one configured in HUB (example 300)
vlan = x