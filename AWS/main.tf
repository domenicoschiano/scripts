## DEFINE PROVIDER
provider "aws" {
  region     = "eu-west-1"
  access_key = var.access_key
  secret_key = var.secret_key
  token      = var.token
}

## DEFINE MAIN VPC
resource "aws_vpc" "terraform-vpc" {
  cidr_block       = var.vpc_subnet_cidr
  enable_dns_hostnames = true
    tags = {
    Name = "terraform-test"
  }
}

## DEFINE SUBNET
resource "aws_subnet" "terraform-subnet" {
  vpc_id     = aws_vpc.terraform-vpc.id
  cidr_block = var.private_subnet_cidr

  tags = {
    Name = "terraform-subnet"
  }
}

## DEFINE TRANSIT GATEWAY
resource "aws_ec2_transit_gateway" "terraform-tgw" {
  amazon_side_asn = 64532 

  tags = {
    Name = "terraform-gateway"
  }
}

## DEFINE VPC TO ATTACH TO TRANSIT GATEWAY
resource "aws_ec2_transit_gateway_vpc_attachment" "terraform-attach" {
  subnet_ids         = [aws_subnet.terraform-subnet.id]
  transit_gateway_id = aws_ec2_transit_gateway.terraform-tgw.id
  vpc_id             = aws_vpc.terraform-vpc.id

 tags = {
    Name = "terraform-attach"
  }
}

## DEFINE DIRECT CONNECTION 
resource "aws_dx_connection" "terraform-dircon" {
  name      = "tf-dx-connection"
  bandwidth = "10Gbps"
  location  = "EirCL"

tags = {
    Name = "terraform-direct-connection"
  }
}

## DEFINE DIRECT GATEWAY 
resource "aws_dx_gateway" "terraform-direct-gateway" {
  name            = "terraform-direct-gateway"
  amazon_side_asn = "64512"
}

## DEFINE DIRECT ASSOCIATE CONNECTION
resource "aws_dx_gateway_association" "transit" {
  dx_gateway_id         = aws_dx_gateway.terraform-direct-gateway.id
  associated_gateway_id = aws_ec2_transit_gateway.terraform-tgw.id

allowed_prefixes = [
    var.private_subnet_cidr,
    
  ]
} 


## DEFINE DIRECT CONNECTION VIRTUAL LINK
resource "aws_dx_transit_virtual_interface" "terraform-vlink" {
  connection_id =  aws_dx_connection.terraform-dircon.id

  name             = "terraform-customer"
  vlan             = var.vlan 
  address_family   = "ipv4"
  bgp_asn          = var.bgp_asn
  amazon_address   = var.primary_amazon_address 
  mtu              = var.mtu
  bgp_auth_key     = "MuLt1Cl0Ud"
  customer_address = var.primary_customer_address
  dx_gateway_id    = aws_dx_gateway.terraform-direct-gateway.id
}