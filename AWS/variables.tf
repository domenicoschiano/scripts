variable "access_key" {
  description = "access key to create instances"
  
}

variable "secret_key" {
  description = "secret key to create instances"
  
}

variable "token" {
  description = "token to create instances"
  
}

variable "vpc_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    type = string
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    type = string
}

variable "primary_amazon_address" {
    description = "AWS CUSTOMER BGP"
    type = string
}

variable "primary_customer_address" {
    description = "HUB CUSTOMER BGP Peer"
    type = string
}

variable "mtu" {
    description = "MTU for Virtual link"
    type = string
}

variable "bgp_asn" {
    description = "AWS BGP ASN"
    type = string
}

variable "vlan" {
    description = "Dot.1Q for VLINK"
    type = string
}

